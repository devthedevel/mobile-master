package devthedevel.xyz.mobilemaster.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import devthedevel.xyz.mobilemaster.database.entities.Name

/**
 * Created by devin on 1/21/2019
 **/
@Dao
interface NameDao {

    @Query("SELECT * FROM names")
    fun findAll(): LiveData<List<Name>>

    @Query("SELECT * FROM names WHERE gender = :gender AND origin = :origin UNION SELECT * FROM names WHERE gender IS NULL AND origin = :origin AND isSurname = :isSurname")
    fun findByParams(gender: String, origin: String, isSurname: Boolean): LiveData<List<Name>>

    @Insert
    fun insertOne(name: Name)

    @Insert
    fun insert(vararg names: Name)

    @Update
    fun update(name: Name)

    @Delete
    fun delete(name: Name)
}