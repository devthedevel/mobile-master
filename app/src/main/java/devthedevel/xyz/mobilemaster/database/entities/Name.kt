package devthedevel.xyz.mobilemaster.database.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by devin on 1/21/2019
 **/
@Entity(tableName = "names")
data class Name(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    var name: String,
    var gender: String?,
    var origin: String,
    var isSurname: Boolean = false
)