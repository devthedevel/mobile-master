package devthedevel.xyz.mobilemaster.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import devthedevel.xyz.mobilemaster.database.dao.NameDao
import devthedevel.xyz.mobilemaster.database.entities.Name

/**
 * Created by devin on 1/21/2019
 **/
@Database(entities = arrayOf(Name::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun nameDao(): NameDao
}