package devthedevel.xyz.mobilemaster

import android.app.Application
import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import devthedevel.xyz.mobilemaster.database.AppDatabase
import devthedevel.xyz.mobilemaster.database.entities.Name
import java.util.concurrent.Executors

/**
 * Created by devin on 1/21/2019
 **/
class App : Application() {

    companion object {
        lateinit var db: AppDatabase
    }

    override fun onCreate() {
        super.onCreate()
        db = Room.databaseBuilder(this, AppDatabase::class.java, "mobile_master").addCallback(object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                Executors.newSingleThreadScheduledExecutor().execute {
                    App.db.nameDao().insert(
                        Name(name = "human male", gender = "Male", origin = "Human"),
                        Name(name = "human female", gender = "Female", origin = "Human"),
                        Name(name = "human any", gender = "Male", origin = "Human"),
                        Name(name = "human any", gender = "Female", origin = "Human"),
                        Name(name = "orc male", gender = "Male", origin = "Orc"),
                        Name(name = "any", gender = "Male", origin = "Human"),
                        Name(name = "any", gender = "Female", origin = "Human"),
                        Name(name = "any", gender = "Male", origin = "Orc"),
                        Name(name = "any", gender = "Female", origin = "Orc"),
                        Name(name = "sur any 1", gender = null, origin = "Human", isSurname = true),
                        Name(name = "sur any 2", gender = null, origin = "Human", isSurname = true),
                        Name(name = "sur any 1", gender = null, origin = "Orc", isSurname = true),
                        Name(name = "sur any 2", gender = null, origin = "Orc", isSurname = true),
                        Name(name = "sur human", gender = null, origin = "Human", isSurname = true),
                        Name(name = "sur orc", gender = null, origin = "Orc", isSurname = true))
                }
            }
        }).build()
    }
}