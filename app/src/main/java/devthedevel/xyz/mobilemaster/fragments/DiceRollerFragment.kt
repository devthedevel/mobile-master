package devthedevel.xyz.mobilemaster.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import devthedevel.xyz.mobilemaster.R
import kotlinx.android.synthetic.main.fragment_dice_roller.*
import kotlin.random.Random

/**
 * Created by devin on 1/21/2019
 *
 * <p> Fragment to roll dice </p>
 **/
class DiceRollerFragment : DialogFragment() {

    private lateinit var rootView: View

    private val d4Count: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    private val d6Count: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    private val d10Count: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    private val d12Count: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    private val d20Count: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    private val d100Count: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    private val modCount: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    private val d4Result: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    private val d6Result: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    private val d10Result: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    private val d12Result: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    private val d20Result: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    private val d100Result: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    private val total: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>().also { it.value = 0 }
    }

    companion object {
        /**
         * Create a new instance of DiceRollerFragment
         */
        fun newInstance(): DiceRollerFragment {
            return DiceRollerFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_roller_d4_dec.setOnClickListener {
            d4Count.value?.run {
                if (this > 0) {
                    d4Count.value = this - 1
                }
            }
        }

        btn_roller_d4_inc.setOnClickListener {
            d4Count.setValue(d4Count.value?.plus(1))
        }

        btn_roller_d6_dec.setOnClickListener {
            d6Count.value?.run {
                if (this > 0) {
                    d6Count.value = this - 1
                }
            }
        }

        btn_roller_d6_inc.setOnClickListener {
            d6Count.setValue(d6Count.value?.plus(1))
        }

        btn_roller_d10_dec.setOnClickListener {
            d10Count.value?.run {
                if (this > 0) {
                    d10Count.value = this - 1
                }
            }
        }

        btn_roller_d10_inc.setOnClickListener {
            d10Count.setValue(d10Count.value?.plus(1))
        }

        btn_roller_d12_dec.setOnClickListener {
            d12Count.value?.run {
                if (this > 0) {
                    d12Count.value = this - 1
                }
            }
        }

        btn_roller_d12_inc.setOnClickListener {
            d12Count.setValue(d12Count.value?.plus(1))
        }

        btn_roller_d20_dec.setOnClickListener {
            d20Count.value?.run {
                if (this > 0) {
                    d20Count.value = this - 1
                }
            }
        }

        btn_roller_d20_inc.setOnClickListener {
            d20Count.setValue(d20Count.value?.plus(1))
        }

        btn_roller_d100_dec.setOnClickListener {
            d100Count.value?.run {
                if (this > 0) {
                    d100Count.value = this - 1
                }
            }
        }

        btn_roller_d100_inc.setOnClickListener {
            d100Count.setValue(d100Count.value?.plus(1))
        }

        btn_roller_mod_dec.setOnClickListener {
            modCount.setValue(modCount.value?.minus(1))
        }

        btn_roller_mod_inc.setOnClickListener {
            modCount.setValue(modCount.value?.plus(1))
        }

        btn_roller_roll.setOnClickListener { onRoll() }

        btn_roller_clear.setOnClickListener {
            d4Count.value = 0
            d6Count.value = 0
            d10Count.value = 0
            d12Count.value = 0
            d20Count.value = 0
            d100Count.value = 0
            modCount.value = 0
            d4Result.value = 0
            d6Result.value = 0
            d10Result.value = 0
            d12Result.value = 0
            d20Result.value = 0
            d100Result.value = 0
            total.value = 0
        }

        d4Count.observe(this, Observer<Int> { text_roller_d4_count?.text = it.toString() })
        d6Count.observe(this, Observer<Int> { text_roller_d6_count?.text = it.toString() })
        d10Count.observe(this, Observer<Int> { text_roller_d10_count?.text = it.toString() })
        d12Count.observe(this, Observer<Int> { text_roller_d12_count?.text = it.toString() })
        d20Count.observe(this, Observer<Int> { text_roller_d20_count?.text = it.toString() })
        d100Count.observe(this, Observer<Int> { text_roller_d100_count?.text = it.toString() })
        modCount.observe(this, Observer<Int> { text_roller_mod_count?.text = it.toString() })
        d4Result.observe(this, Observer<Int> { text_roller_d4_result?.text = it.toString() })
        d6Result.observe(this, Observer<Int> { text_roller_d6_result?.text = it.toString() })
        d10Result.observe(this, Observer<Int> { text_roller_d10_result?.text = it.toString() })
        d12Result.observe(this, Observer<Int> { text_roller_d12_result?.text = it.toString() })
        d20Result.observe(this, Observer<Int> { text_roller_d20_result?.text = it.toString() })
        d100Result.observe(this, Observer<Int> { text_roller_d100_result?.text = it.toString() })
        total.observe(this, Observer<Int> { text_roller_total?.text = it.toString() })
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            rootView = activity!!.layoutInflater.inflate(R.layout.fragment_dice_roller, @Suppress null)

            AlertDialog.Builder(it).setCancelable(true).setView(rootView).create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    /**
     * Handle calculating all the dice in the pool, and update UI.
     */
    private fun onRoll() {
        var accumulatedTotal = 0
        d4Count.value?.let {
            val result = calculateDiceRoll(it, 4)
            d4Result.value = result
            accumulatedTotal += result
        }

        d6Count.value?.let {
            val result = calculateDiceRoll(it, 6)
            d6Result.value = result
            accumulatedTotal += result
        }

        d10Count.value?.let {
            val result = calculateDiceRoll(it, 10)
            d10Result.value = result
            accumulatedTotal += result
        }

        d12Count.value?.let {
            val result = calculateDiceRoll(it, 12)
            d12Result.value = result
            accumulatedTotal += result
        }

        d20Count.value?.let {
            val result = calculateDiceRoll(it, 20)
            d20Result.value = result
            accumulatedTotal += result
        }

        d100Count.value?.let {
            val result = calculateDiceRoll(it, 100)
            d100Result.value = result
            accumulatedTotal += result
        }

        modCount.value?.let { accumulatedTotal += it }

        total.value = accumulatedTotal
    }

    /**
     * Calculate a dice roll.
     * @param numDice Number of dice to use
     * @param limit The die's limit (d6's limit would be 6)
     * @return The result of the dice roll
     */
    private fun calculateDiceRoll(numDice: Int, limit: Int): Int {
        var result = 0
        for (i in 0 until numDice) {
            result += Random.nextInt(0, limit) + 1
        }
        return result
    }
}