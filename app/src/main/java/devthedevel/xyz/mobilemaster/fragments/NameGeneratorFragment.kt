package devthedevel.xyz.mobilemaster.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import devthedevel.xyz.mobilemaster.App
import devthedevel.xyz.mobilemaster.R
import devthedevel.xyz.mobilemaster.database.entities.Name
import kotlinx.android.synthetic.main.fragment_name_generator.*

/**
 * Created by devin on 1/21/2019
 **/
class NameGeneratorFragment : DialogFragment() {

    private lateinit var rootView: View

    private lateinit var paramGender: String
    private lateinit var paramOrigin: String
    private var paramSurname = false

    companion object {
        fun newInstance(): NameGeneratorFragment {
            return NameGeneratorFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ArrayAdapter.createFromResource(context, R.array.array_genders, android.R.layout.simple_spinner_item).also { arrayAdapter ->
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_name_gen_gender.adapter = arrayAdapter
            spinner_name_gen_gender.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) { }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    paramGender = parent?.getItemAtPosition(position) as String
                }
            }
        }

        ArrayAdapter.createFromResource(context, R.array.array_name_origins, android.R.layout.simple_spinner_item).also { arrayAdapter ->
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_name_gen_origin.adapter = arrayAdapter
            spinner_name_gen_origin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) { }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    paramOrigin = parent?.getItemAtPosition(position) as String
                }
            }
        }

        sw_name_gen_surname.setOnCheckedChangeListener { _, isChecked ->
            paramSurname = isChecked
        }

        btn_name_gen_gen.setOnClickListener {
            val observer: LiveData<List<Name>> = App.db.nameDao().findByParams(paramGender, paramOrigin, paramSurname)

            observer.observe(this, Observer {
                it?.run {
                    text_name_gen_name?.text = if (isEmpty()) {
                        getString(R.string.text_no_name_available)
                    } else {
                        val name = filterNot { it.isSurname }.random()

                        if (paramSurname) {
                            val surnames = filter { it.isSurname }

                            if (surnames.isNotEmpty()) {
                                name.name += " ${surnames.random().name}"
                            }
                        }

                        name.name
                    }
                }
            })
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            rootView = activity!!.layoutInflater.inflate(R.layout.fragment_name_generator, @Suppress null)

            AlertDialog.Builder(it).setCancelable(true).setView(rootView).create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

}