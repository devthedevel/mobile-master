package devthedevel.xyz.mobilemaster.fragments

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import devthedevel.xyz.mobilemaster.App
import devthedevel.xyz.mobilemaster.R
import devthedevel.xyz.mobilemaster.database.entities.Name
import kotlinx.android.synthetic.main.fragment_database_names.*

/**
 * Created by devin on 1/26/2019
 **/
class DatabaseNamesFragment : Fragment() {

    companion object {
        private const val TEXT_SIZE = 18f
        private val ROW_PARAMS = TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT)
        private val VIEW_PARAMS = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1f)

        fun newInstance(): DatabaseNamesFragment {
            return DatabaseNamesFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return activity?.let {
            return activity!!.layoutInflater.inflate(R.layout.fragment_database_names, @Suppress null)
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val observer = Observer<List<Name>> {
            it?.run {

                val list = sortedWith(compareBy ({ it.name }, {it.gender}))

                for (name: Name in list) {
                    val row = TableRow(context)
                    row.layoutParams = ROW_PARAMS

                    val tvName = TextView(context)
                    tvName.layoutParams = VIEW_PARAMS
                    tvName.text = name.name
                    tvName.textSize = TEXT_SIZE

                    val tvGender = TextView(context)
                    tvGender.layoutParams = VIEW_PARAMS
                    tvGender.text = name.gender
                    tvGender.textSize = TEXT_SIZE
//                    tvGender.gravity = Gravity.CENTER

                    val tvOrigin = TextView(context)
                    tvOrigin.layoutParams = VIEW_PARAMS
                    tvOrigin.text = name.origin
                    tvOrigin.textSize = TEXT_SIZE
//                    tvOrigin.gravity = Gravity.CENTER

                    val tvSurname = TextView(context)
                    tvSurname.layoutParams = VIEW_PARAMS
                    tvSurname.text = name.isSurname.toString()
                    tvSurname.textSize = TEXT_SIZE

                    row.addView(tvName)
                    row.addView(tvGender)
                    row.addView(tvOrigin)
                    row.addView(tvSurname)
                    database_names_table.addView(row)
                }
            }
        }

        App.db.nameDao().findAll().observe(this, observer)
    }
}